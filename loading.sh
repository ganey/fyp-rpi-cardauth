#!/bin/bash
shopt -s expand_aliases
alias selgrn="gpio mode 0 out"
alias selred="gpio mode 1 out"
alias gwrite="gpio write"
for i in {1..20}
do
	selgrn
	gwrite 0 1
	selred
	gwrite 1 1
	selgrn
	gwrite 0 0
	selred
	gwrite 1 0
done
exit 0

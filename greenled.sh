#!/bin/bash

#shopt -s expand_aliases
#alias selgrn="gpio mode 0 out"
#alias selred="gpio mode 1 out"
#alias ledon="gpio write 0 1"
#alias ledoff="gpio write 0 0"

#select green on gpio 0
gpio mode 0 out
#turn green on
gpio write 0 1
#sleep for 1 second
sleep 1
#turn led off
gpio write 0 0
exit 0

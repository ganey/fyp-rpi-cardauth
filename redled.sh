#!/bin/bash

#shopt -s expand_aliases
#alias selgrn="gpio mode 0 out"
#alias selred="gpio mode 1 out"
#alias ledon="gpio write 0 1"
#alias ledoff="gpio write 0 0"

#select red on gpio 1
gpio mode 1 out
#turn green on
gpio write 1 1
#sleep for 1 second
sleep 1
#turn led off
gpio write 1 0
exit 0

import json, requests, ConfigParser, subprocess

#grab info from config
localurl  = '/etc/membermanagement/'
config = ConfigParser.ConfigParser()
config.read(localurl+"managementpanel.cnf")
url	  = config.get('server', 'host')
access_key = config.get('server', 'key')
readerid  = config.get('reader', 'id')
serviceid = config.get('reader', 'service')
authurl  = 'http://'+url+'/reader/cardlogin'

subprocess.Popen(localurl+'loading.sh')
def redled():
	#something to light red led here
	p = subprocess.Popen(localurl+'redled.sh')
def greenled():
	#something to light green led here
	p = subprocess.Popen(localurl+'greenled.sh')
def main():
    	#main functions here
	input_val = raw_input("Scan Card: ")

	params = dict(
    		accesskey=access_key,
    		cardval=input_val,
		service=serviceid
	)

	resp = requests.get(url=authurl, params=params)
	data = json.loads(resp.content)

	print data

	if (data['status']==1):
		greenled()	
	else:
		redled()
	#back to normal
	main()
main()

<?php
$filename = '/etc/membermanagement/managementpanel.cnf';
//$filename = $_SERVER['DOCUMENT_ROOT'].'../managementpanel.cnf';
if (file_exists($filename)) {
	header("HTTP/1.0 403 Forbidden");
    	echo "<div style='text-align:center;margin-top:20%;'><h1>Already Configured.</h1><p>Re-install to reconfigure.</p></div>";
} else {
    	if(isset($_POST['submit']) && isset($_POST['access_key']) && ctype_alnum($_POST['access_key'])) {
		echo "<div style='text-align:center;margin-top:20%;'><h1>Configuring Reader...</h1><p>Please wait while the reader restarts.</p></div>";
		$access_key = $_POST["access_key"];
		//touch($filename);
    		//chmod($filename, 0777);
		$f = @fopen($filename, 'x') or die("<div style='text-align:center;color:red;'>Can't write config file</div>");
    		fwrite($f, "# Automatically generated for Debian scripts. DO NOT TOUCH!\n[server]\nhost       = fyp.ganey.co.uk\nkey        = ".$access_key."\n[reader]\nid         = 1\nservice    = 1");
    		fclose($f);
		system("/etc/membermanagement/reboot.sh");
	}
	else {
?>
<!DOCTYPE>
<html>
<head>
<title>One Time Configuration</title>
</head>
<body>
<div style="margin-top:20%;text-align:center;">
<h1>One Time Configuration</h1>
<p>Please copy the access key from your management panel and paste it here.</p>
<p style="color:red;font-weight:700;font-size:small;">You can only do this once! To reconfigure you must re-install this reader.</p>
<form method="post" name="config_form">
        <div id="field">
            <label>Access Key</label>
            <br />
            <input type="password" name="access_key">
        </div>
	<input type="submit" name="submit" value="Setup this reader!" />

</form>
</div>
</body>
</html>
<?php
	}//if post
}//if configured
?>
